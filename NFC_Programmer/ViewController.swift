//
//  ViewController.swift
//  NFC_Programmer
//
//  Created by Brian Velazquez on 9/3/20.
//  Copyright © 2020 Brian Velazquez. All rights reserved.
//

import UIKit
import CoreNFC

class ViewController: UIViewController, NFCNDEFReaderSessionDelegate {
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        <#code#>
    }
    
    var session: NFCNDEFReaderSession?
    var TheActualMessage = ""
    @IBOutlet weak var nfcMessage: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func writeNCF(_ sender: Any) {
        TheActualMessage = nfcMessage.text!
        session = NFCNDEFReaderSession(delegate:self, queue: nil, invalidateAfterFirstRead: true)
        session?.alertMessage = "Hold Your iPhone near an NCF tag to write a message."
        session?.begin()
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        <#code#>
    }
    func readerSession(_ session: NFCNDEFReaderSession, didDetect tags: [NFCNDEFTag]) {
        
        var str:String = "\(TheActualMessage)"
        var strToUint8:[UInt8] = [UInt8](from: str.t=UTF8 as! Decoder as! Decoder)
    }
       
    
}

